
import time;
width = 5;
height = 5;
#grounds = [[0]*width for i in range(height)];
grounds= [
		[0,0,0,0,0,0],
		[0,1,1,0,0,0],
		[0,1,1,0,0,0],
		[0,0,0,1,1,0],
		[0,0,0,1,1,0],
			];


def getNumberOfNeighbours (x, y, matrix):
	n = 0;
	if (x != 0 and matrix[y][x - 1] == 1):
		n = n + 1;
	if (y != 0 and matrix[y - 1][x] == 1):
		n = n + 1;
	if (y + 1 < len(matrix) and matrix[y + 1][x] == 1):
		n = n + 1;
	if (x + 1 < len(matrix[0]) and matrix[y][x + 1] == 1):
		n = n + 1;
	if (x + 1 < len(matrix[0]) and y + 1 < len(matrix) and matrix[y + 1][x + 1] == 1):
		n = n + 1;
	if (x != 0 and y != 0 and matrix[y - 1][x - 1] == 1):
		n = n + 1;
	if (x + 1 < len(matrix[0]) and y != 0 and matrix[y - 1][x + 1] == 1):
		n = n + 1;
	if (y + 1 < len(matrix) and x != 0 and matrix[y + 1][x - 1] == 1):
		n = n + 1;
	return n;
	
def processGrounds(matrix):
	newMatrix = [[0]*len(matrix[0]) for i in range(len(matrix))];
	for y in range(len(matrix)):
			for x in range(len(matrix[0])):
				n = getNumberOfNeighbours(x,y,matrix);
				if (n < 2 and matrix[y][x] == 1):
					newMatrix[y][x] = 0;
				elif ((n == 2 or n == 3) and matrix[y][x] == 1):
					newMatrix[y][x] = 1;
				elif (n > 3 and matrix[y][x] == 1):
					newMatrix[y][x] = 0;
				elif (n == 3 and matrix[y][x] == 0):
					newMatrix[y][x] = 1;
	
	return newMatrix;

while(True):
	grounds = processGrounds(grounds);
	for y in range(len(grounds)):
		print(grounds[y]);
		
	print('');
	time.sleep(0.5);

